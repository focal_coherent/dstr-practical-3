import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.Scanner;

public class Student {
    private String adminNo, name;
    private GregorianCalendar birthDate;
    private int test1, test2, test3;

    public Student(String adminNo, String name, String birthDate, int test1, int test2, int test3) {
        this.adminNo = adminNo;
        this.name = name;
        this.birthDate = MyCalendar.convertDate(birthDate);
        this.test1 = test1;
        this.test2 = test2;
        this.test3 = test3;
    }

    public Student(String studentRecord) {
        Scanner sc = new Scanner(studentRecord);
        sc.useDelimiter(";");
        this.adminNo = sc.next();
        this.name = sc.next();
        this.birthDate = MyCalendar.convertDate(sc.next());
        this.test1 = sc.nextInt();
        this.test2 = sc.nextInt();
        this.test3 = sc.nextInt();
    }

    public String getAdminNo() {
        return adminNo;
    }

    public void setAdminNo(String adminNo) {
        this.adminNo = adminNo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public GregorianCalendar getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = MyCalendar.convertDate(birthDate);
    }

    public int getTest1() {
        return test1;
    }

    public void setTest1(int test1) {
        this.test1 = test1;
    }

    public int getTest2() {
        return test2;
    }

    public void setTest2(int test2) {
        this.test2 = test2;
    }

    public int getTest3() {
        return test3;
    }

    public void setTest3(int test3) {
        this.test3 = test3;
    }

    public double getAverageScore() {
        return ((double) test1 + test2 + test3) / 3.0;
    }

    @Override
    public String toString() {
        return "Student [adminNo=" + adminNo + ", name=" + name + 
                ", birthday=" + MyCalendar.formatDate(birthDate) +
                ", getAverageScore()= " + getAverageScore() + "]";
    }

    public static ArrayList<Student> readStudent(String file) {
        // declare and instantiate a FileController object using input parameter file
        FileController fc = new FileController(file);

        ArrayList<Student> recs = new ArrayList<Student>();
        ArrayList<String> recsReturn;

        // invoke the readLine methods in FileController to read
        // records from file and stores in variable recsReturn
        recsReturn = fc.readLine();

        // write a for loop to create student objects and
        // store them in arraylist recs
        Student stu;
        for (String s : recsReturn) {
            stu = new Student(s);
            recs.add(stu);
        }

        return recs;
    }

    public static void writeStudent(ArrayList<Student> arrl, String file) {        
        ArrayList<String> parsedStudents = new ArrayList<>();
        for (Student stu : arrl) {
            parsedStudents.add(stu.adminNo +";" + stu.name + ";" + 
                           MyCalendar.formatDate(stu.getBirthDate()) + ";" +
                           stu.getTest1() + ";" + stu.getTest2() + ";" + stu.getTest3());
        }
        
        FileController fc = new FileController(file);
        fc.writeLine(parsedStudents);

    }

}