import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

public class FileController {
    private String fileName;

    public FileController(String fileName) {
        this.fileName = fileName;
    }

    public ArrayList<String> readLine() {
        ArrayList<String> ret = new ArrayList<>();
        try {
            FileReader fr = new FileReader(fileName);
            Scanner sc = new Scanner(fr);
            while (sc.hasNextLine()) {
                ret.add(sc.nextLine());
            }
            sc.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return ret;
    }

    public void writeLine(String s) {
        try {
            FileWriter fw = new FileWriter(fileName);
            BufferedWriter bw = new BufferedWriter(fw);
            PrintWriter outFile = new PrintWriter(bw);

            outFile.println(s);
            outFile.close();
        } catch (IOException e) {
            e.printStackTrace();
        } 
    }

    public void writeLine(ArrayList<String> arrl) {
        try {
            FileWriter fw = new FileWriter(fileName);
            BufferedWriter bw = new BufferedWriter(fw);
            PrintWriter outFile = new PrintWriter(bw);

            for (String s : arrl) {
                outFile.println(s);
            }
            outFile.close();
        } catch (IOException e) {
            e.printStackTrace();
        } 
    }

}