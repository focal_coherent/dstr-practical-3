import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;

public class Question3Controller implements Initializable {
	
	@FXML private ChoiceBox<String> adminNo;
	@FXML private TextField nameField, birthDateField, test1, test2, test3; 
	
	private ArrayList<Student> students = new ArrayList<Student>();
	private int currIndex;

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		students = Student.readStudent("student.txt");
		
		for (Student s : students) {
			adminNo.getItems().add(s.getAdminNo());
		}
		
		adminNo.getSelectionModel().selectFirst();
				
	}
	
	@FXML
	public void getStudent(ActionEvent e) {
		currIndex = adminNo.getSelectionModel().getSelectedIndex();
		nameField.setText(students.get(currIndex).getName());
		birthDateField.setText(MyCalendar.formatDate(students.get(currIndex).getBirthDate()));
		test1.setText(""+students.get(currIndex).getTest1());
		test2.setText(""+students.get(currIndex).getTest2());
		test3.setText(""+students.get(currIndex).getTest3());
	}
	
	@FXML
	public void submit(ActionEvent e) {
		students.get(currIndex).setName(nameField.getText());
		students.get(currIndex).setBirthDate(birthDateField.getText());
		students.get(currIndex).setTest1(Integer.parseInt(test1.getText()));
		students.get(currIndex).setTest2(Integer.parseInt(test2.getText()));
		students.get(currIndex).setTest3(Integer.parseInt(test3.getText()));
		
		Student.writeStudent(students, "student.txt");
	}
	
	@FXML 
	public void exit(ActionEvent e) {
		Platform.exit();
	}
	
}
