import java.io.*;
import java.util.*;

public class StudentApp {
    public static void main(String[] args) {
         // declare and instantiate a FileController object
         // for text file "student.txt"

        FileController fc = new FileController("student.txt");
        ArrayList<String> recs = new ArrayList<>();
        
        recs.add("031111A;Mary Tan;1/06/1981;100;100;90");
        recs.add("031111B;Joshua Tan;12/07/1981;70;80;90");
        recs.add("031111C;Jim Kwik;5/03/1981;90;100;90");

        // invoke the writeLine method in FileController 
        // to write records to file
        fc.writeLine(recs);

        // invoke the readLine methods in FileController to read
        // records from file and stores the records in variable recsReturn
        ArrayList<String> recsReturn = fc.readLine();

        Student stu;
        for (String s : recsReturn) {
            stu = new Student(s);
            System.out.println(stu.getName());
        }

        System.out.println();

        ArrayList<Student> students = Student.readStudent("student.txt");
        for (Student s : students) {
            System.out.println(s.getName());
        }

    }
}