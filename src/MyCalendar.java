/*===================================================================
Program Name:	MyCalendar.java
Description:	A class to perform date functions
Done by:	Phoon Lee Kien
Admin No:	001234A
Module Group:	IT2525-99
=====================================================================*/

import java.util.*;

public class MyCalendar {

	//Get the number of days for first date minus second date
	public static long getDifference (GregorianCalendar d1, 
								GregorianCalendar d2){
									
		long firstDate = d1.getTimeInMillis();
		long secDate = d2.getTimeInMillis();
		
		return (firstDate - secDate)/(24*60*60*1000);
	}
	public static String formatDate (GregorianCalendar d1){
		int day = d1.get(Calendar.DATE);
		int month = d1.get(Calendar.MONTH)+ 1 ;
		int year = d1.get(Calendar.YEAR);
		return day + "/" + month + "/" + year;
	}

	public static GregorianCalendar convertDate(String s) {
		String[] fields = s.split("/"); // {"dd", "mm", "yyyy"}
		int dayOfMonth = Integer.parseInt(fields[0]);
		int month = Integer.parseInt(fields[1]);
		int year = Integer.parseInt(fields[2]);
		
		return new GregorianCalendar(year, month-1, dayOfMonth);
		// month starts from 0, hence minus 1
	} 
	
}

